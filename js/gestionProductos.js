
let productosObtenidos;

function getProductos() {
  let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  let request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarProductos(){
  let jsonProductos = JSON.parse(productosObtenidos);
  let divTabla = document.getElementById("divProductos");
  divTabla.innerHTML = '';

  let tabla = document.createElement("table");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  let tbody = document.createElement("tbody");

  for (var i = 0; i < jsonProductos.value.length; i++) {
    let nuevaFila =  document.createElement("tr");

    let columnaNombre =  document.createElement("td");
    columnaNombre.innerText = jsonProductos.value[i].ProductName;
    nuevaFila.appendChild(columnaNombre);

    let columnaPrecio =  document.createElement("td");
    columnaPrecio.innerText = jsonProductos.value[i].UnitPrice;
    nuevaFila.appendChild(columnaPrecio);

    let columnaStock =  document.createElement("td");
    columnaStock.innerText = jsonProductos.value[i].UnitsInStock;
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
    console.log(jsonProductos.value[i].ProductID +"\t"+ jsonProductos.value[i].ProductName) ;
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
