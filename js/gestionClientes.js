let clientesObtenidos;

function getClientes() {
  let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  //let params = "?$filter=Country eq 'Germany'";
  let request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  let rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  let jsonClientes = JSON.parse(clientesObtenidos);
  let divTabla = document.getElementById("divClientes");
  divTabla.innerHTML = '';

  let tabla = document.createElement("table");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  let tbody = document.createElement("tbody");

  for (var i = 0; i < jsonClientes.value.length; i++) {
    let nuevaFila =  document.createElement("tr");

    let columnaNombre =  document.createElement("td");
    columnaNombre.innerText = jsonClientes.value[i].ContactName;
    nuevaFila.appendChild(columnaNombre);

    let columnaCiudad =  document.createElement("td");
    columnaCiudad.innerText = jsonClientes.value[i].City;
    nuevaFila.appendChild(columnaCiudad);

    let imgBandera = document.createElement("img");
    imgBandera.classList.add  ("flag");

    if(jsonClientes.value[i].Country === "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    } else {
      imgBandera.src = rutaBandera + jsonClientes.value[i].Country  + ".png";
    }
    let columnaBandera =  document.createElement("td");

    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
